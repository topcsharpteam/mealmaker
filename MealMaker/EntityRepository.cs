﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MealMaker
{
    class EntityRepository
    {

        private List<Entity> _entities = new List<Entity>();

        public void Read()
        {
            MealMakerEntities db = new MealMakerEntities();

            _entities = db.Entity.ToList<Entity>();
        }

        public List<Entity> Entities
        {
            get { return _entities; }
            set { _entities = value; }
        }
    }
}
