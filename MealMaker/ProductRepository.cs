﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MealMaker
{
    class ProductRepository
    {
        Random rand = new Random();
        private Product _prod;
        private Entity _entity;
        private string _ProdDescr;
        private List<Product> _Prod = new List<Product>();
        private BitmapImage _img;
        private MealMakerEntities db;

        public Action<List<Product>> OnChangeProdList;

        public void Read()
        {
            db = new MealMakerEntities();

            _Prod = db.Product.ToList<Product>();

            OnChangeProdList?.Invoke(_Prod);
        }

        public void SelectedEntity(Entity ENTITY)
        {
            _entity = ENTITY;
            db = new MealMakerEntities();

            _Prod = db.Product.Where(ent => ent.Entity_ID == _entity.Entity_ID).ToList();

            OnChangeProdList?.Invoke(_Prod);

        }

        public void FindMatch(Product PROD)
        {
            int pi1;
            int pi2;
            int pi3;

            Product pic1;
            Product pic2;
            Product pic3;

            db = new MealMakerEntities();
            _prod = PROD;

            if (_prod.Salinity_ID != 2)
            {
                _Prod = db.Product.Where(nonsweat => nonsweat.Salinity_ID != 2 && nonsweat.Taste_ID == _prod.Taste_ID)
                    .Where(ent=>ent.Entity_ID !=_prod.Entity_ID).ToList();
            }
            else
            _Prod = db.Product.Where(nonsweat => nonsweat.Salinity_ID == 2 && nonsweat.Taste_ID == _prod.Taste_ID)
                    .Where(ent=>ent.Entity_ID !=_prod.Entity_ID).ToList();

            if (_Prod.Count > 3)
            {
                do
                {
                    pi1 = rand.Next(0,_Prod.Count- 1);
                    pi2 = rand.Next(0, _Prod.Count - 1);
                    pi3 = rand.Next(0, _Prod.Count - 1);

                    pic1 = _Prod[pi1];
                    pic2 = _Prod[pi2];
                    pic3 = _Prod[pi3];

                } while (pi1 == pi2 || pi1 == pi3 || pi2 == pi3);

                _Prod[0] = pic1;
                _Prod[1] = pic2;
                _Prod[2] = pic3;

            }
        }
            
           
                  


        public List<Product> MatchedProd
        {
            get
            { return _Prod; }
            set
            { _Prod = value; }
        }


        public void Get_Info(Product PROD)
        {
            _prod = PROD;
            _ProdDescr = _prod.Product_descr.ToString();
            string SelectedProduct_pic = _prod.Product_pic;
            _img = new BitmapImage(new Uri(SelectedProduct_pic.Trim(new char[] { ' ', '.' }), UriKind.Relative));

            

        }

        public BitmapImage IMG
        {
            get
            { return _img; }
            set
            { _img = value; }
        }
        
        public string Descr
        {
            get
            { return _ProdDescr; }
            set
            { _ProdDescr = value; }
        }
    }
}

