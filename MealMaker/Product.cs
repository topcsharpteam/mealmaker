//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MealMaker
{
    using System;
    using System.Collections.Generic;
    
    public partial class Product
    {
        public int Product_ID { get; set; }
        public string Product_name { get; set; }
        public string Product_descr { get; set; }
        public string Product_pic { get; set; }
        public int Entity_ID { get; set; }
        public int Taste_ID { get; set; }
        public int Salinity_ID { get; set; }
    
        public virtual Entity Entity { get; set; }
        public virtual Salinity Salinity { get; set; }
        public virtual Taste Taste { get; set; }
    }
}
