﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace MealMaker
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {

        ProductRepository _productRepo = new ProductRepository();
        EntityRepository _entityRepo = new EntityRepository();

        MealMakerEntities db = new MealMakerEntities();

        Product _prod = new Product();
        List<Product> ghostProd = new List<Product>();

        List<Entity> Ent = new List<Entity>();

        public MainWindow()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();

            _productRepo.OnChangeProdList += _prod => ProductsList.ItemsSource = _prod;
            _productRepo.Read();
            _entityRepo.Read();
            ComboBoxCategories.ItemsSource = _entityRepo.Entities;

           

        }

        private void ComboItemSelected(object sender, SelectionChangedEventArgs e) //При выборе категории будут отображаться продукты только из этой категории
        {
            _productRepo.SelectedEntity(ComboBoxCategories.SelectedItem as Entity);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PopUpInfo popa = new PopUpInfo();
            popa.Show();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(ProductsList.SelectedItem != null)
            {
                ghostProd.Clear();

                FirstIMG.Source = null;
                SecondIMG.Source = null;
                ThirdIMG.Source = null;

                _productRepo.Get_Info(ProductsList.SelectedItem as Product);
                ProductImg.Source = _productRepo.IMG;
                ProdDescr.Text = _productRepo.Descr;
                LabelMatch.Visibility = Visibility.Visible; //Еее, мы снова видим ети лейблы!

                _productRepo.FindMatch(ProductsList.SelectedItem as Product);
                List<Product> foundProds;
                ghostProd.AddRange(foundProds = _productRepo.MatchedProd);

                string SelectedProduct_pic1 = null;
                string SelectedProduct_pic2 = null;
                string SelectedProduct_pic3 = null;
                if (foundProds.Count == 1)
                {
                    SelectedProduct_pic1 = ghostProd[0].Product_pic;

                    BitmapImage img1 = new BitmapImage(new Uri(SelectedProduct_pic1.Trim(new char[] { ' ', '.' }), UriKind.Relative));

                    FirstIMG.Source = img1;
                }
                else if (foundProds.Count == 2)
                {
                    SelectedProduct_pic1 = ghostProd[0].Product_pic;
                    SelectedProduct_pic2 = ghostProd[1].Product_pic;

                    BitmapImage img1 = new BitmapImage(new Uri(SelectedProduct_pic1.Trim(new char[] { ' ', '.' }), UriKind.Relative));
                    BitmapImage img2 = new BitmapImage(new Uri(SelectedProduct_pic2.Trim(new char[] { ' ', '.' }), UriKind.Relative));

                    FirstIMG.Source = img1;
                    SecondIMG.Source = img2;
                }
                else
                {
                    SelectedProduct_pic1 = ghostProd[0].Product_pic;
                    SelectedProduct_pic2 = ghostProd[1].Product_pic;
                    SelectedProduct_pic3 = ghostProd[2].Product_pic;

                    BitmapImage img1 = new BitmapImage(new Uri(SelectedProduct_pic1.Trim(new char[] { ' ', '.' }), UriKind.Relative));
                    BitmapImage img2 = new BitmapImage(new Uri(SelectedProduct_pic2.Trim(new char[] { ' ', '.' }), UriKind.Relative));
                    BitmapImage img3 = new BitmapImage(new Uri(SelectedProduct_pic3.Trim(new char[] { ' ', '.' }), UriKind.Relative));

                    FirstIMG.Source = img1;
                    SecondIMG.Source = img2;
                    ThirdIMG.Source = img3;
                }
            }

        }

        private void Test_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
